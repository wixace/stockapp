﻿using System;
using UnityEngine;

public class UIOptionGroup : MonoBehaviour {

    private UIOptionElement[] _optionElements;

    public static Action<int> OnOptionSelected;

    [SerializeField] private GameObject[] _views;
        
    private int _currentId;
    

    void Awake() {
        _optionElements = GetComponentsInChildren<UIOptionElement>();
        OnOptionSelected+=OptionSelected;
    }

    private void Start() {
        _optionElements[_currentId].Select();
    }

    private void OptionSelected(int i){
        _views[_currentId].SetActive(false);
        _currentId=i;
        _views[_currentId].SetActive(true);
    }

}
