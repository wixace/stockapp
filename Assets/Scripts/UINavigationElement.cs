﻿using UnityEngine;
using UnityEngine.UI;

public class UINavigationElement : MonoBehaviour
{
    private Image _image;

    private Text _text;
    
    private Button _button;
    
    private UINavigationGroup _navigationGroup;
    
    [SerializeField] private GameObject _target;
    
    private void Awake()
    {
        _image = GetComponent<Image>();
        _text = GetComponentInChildren<Text>();
        _navigationGroup = GetComponentInParent<UINavigationGroup>();
        GetComponent<Button>().onClick.AddListener((() =>
        {
            _navigationGroup.Select(this);
        }));
    }

    public void Select()
    {
        _image.color=Color.red;;
        _text.color=Color.red;
        _target.SetActive(true);
    }

    public void Deselect()
    {
        _image.color=Color.black;;
        _text.color=Color.black;
        _target.SetActive(false);
    }
    
}
