﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class Indicator : MonoBehaviour {

public Transform Target,Parent;
public GameObject Prefab;

private List<Toggle> Toggles=new List<Toggle>();
	void Start(){
		ScrollSnap.onPageChange+=OnPageChanged;
		int count=Target.childCount;
for(int i=0;i<count;i++)
{
var go=Instantiate(Prefab,Parent).GetComponent<Toggle>();
go.group=GetComponentInParent<ToggleGroup>();
Toggles.Add(go);
}
Toggles[0].isOn=true;
	}

	void OnPageChanged(int p){
		Toggles[p].isOn=true;	}
}
