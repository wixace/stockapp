﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINavigationGroup : MonoBehaviour
{

    private UINavigationElement _currentSelected;

    void Start()
    {
        _currentSelected = GetComponentInChildren<UINavigationElement>();
        _currentSelected.Select();
    }
    public void Select(UINavigationElement element)
    {
        if (element != _currentSelected)
        {
            _currentSelected?.Deselect();
            _currentSelected = element;
            element.Select();
        }
    }
    
    
}
