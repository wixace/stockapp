﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArticleItem : MonoBehaviour
{
    public Text Title;

    public Text Info;

    public string Content;

    public int Category { get; set; }
    public int Id { get; set; }

    public Button Button;

    void Awake()
    {
        Button.onClick.AddListener((() =>
                 {
            ArticleManager.Instance.LoadArticle(this);
        }));
    }

    public void SetText(string title, string author, string date,string content)
    {
        Title.text = title;
        Info.text = author + " " + date;
        Content = content;
    }
}
