﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOptionElement : MonoBehaviour
{

    private Text _text;

    private Image _image;

    private Button _button;

    [SerializeField] private int _id;

    // Start is called before the first frame update
    void Awake() {
        _text = GetComponentInChildren<Text>();
        _image = GetComponentInChildren<Image>();
        _button=GetComponentInChildren<Button>();
    }

void OnEnable(){
    UIOptionGroup.OnOptionSelected +=OptionChanged;
    Deselect();
}
void OnDisable(){
     UIOptionGroup.OnOptionSelected -=OptionChanged;
}
    void Start(){
        _button.onClick.AddListener(()=>{
        UIOptionGroup.OnOptionSelected(_id);    
        });
    }

public void OptionChanged(int i) {
       if(i==_id)
       Select();
       else
       Deselect();
    }

    public void Select() {
        _text.fontStyle = FontStyle.Bold;
        _text.color = Color.red;
        _image.gameObject.SetActive(true);
    }

    public void Deselect() {
        _text.fontStyle = FontStyle.Normal;
        _text.color = Color.black;
        _image.gameObject.SetActive(false);
    }
}
