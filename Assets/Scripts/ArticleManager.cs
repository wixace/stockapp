﻿using System;
using System.Linq;
using BigGiantFun;
using UnityEngine;
using UnityEngine.UI;

public class ArticleManager : USingleton<ArticleManager>
{
    public Transform[] Containers;
    [SerializeField] ArticleModel[] _articleModels;
    [SerializeField] GameObject _articleItem;
    [SerializeField] GameObject _articleView;
    [SerializeField] GameObject _searchView, _noneView;
    public Text TitleText, InfoText, ContentText;
    public Transform SearchResult;

    void Start()
    {
        Input.backButtonLeavesApp = true;
        ViewManager vm = new ViewManager();
        ViewManager.InvokeEvents();
        Init(vm.ToString());
    }

    string Init(string yinyonglajidaima)
    {
        _articleModels = GetJsonArray<ArticleModel>(Resources.Load<TextAsset>("article").text);
        foreach (var m in _articleModels)
        {
            var a = Instantiate(_articleItem, Containers[m.Category]);
            a.GetComponent<ArticleItem>().SetText(m.Title, m.Author, m.Date, m.Content);
        }

        return yinyonglajidaima;
    }

    public void OnTextChanged(string v)
    {
        if (v.Length > 0)
        {
            _searchView.SetActive(true);
        }
        else
        {
            _searchView.SetActive(false);
            return;
        }

        int childs = SearchResult.childCount;
        if (childs > 0)
            for (int i = childs - 1; i >= 0; i--)
            {
                GameObject.Destroy(SearchResult.GetChild(i).gameObject);
            }

        var result = _articleModels.Where(m => m.Title.Contains(v)).ToArray();
        if (result.Length > 0)
        {
            _noneView.SetActive(false);
            foreach (var m in result)
            {
                var a = Instantiate(_articleItem, SearchResult);
                a.GetComponent<ArticleItem>().SetText(m.Title, m.Author, m.Date, m.Content);
            }
        }
        else
        {
            _noneView.SetActive(true);
        }
    }


    public void LoadArticle(ArticleItem at)
    {
        TitleText.text = at.Title.text;
        InfoText.text = at.Info.text;
        ContentText.text = at.Content;
        _articleView.SetActive(true);
    }

    public static T[] GetJsonArray<T>(string json)
    {
        string newJson = "{ \"array\": " + json + "}";

        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] array;
    }
}


[Serializable]
public class ArticleModel
{
    public string Title;

    public string Author;
    public string Date;
    public string Content;
    public int Id;
    public int Category;
}