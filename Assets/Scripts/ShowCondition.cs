﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShowCondition : MonoBehaviour
{

    public GameObject ErrorView;

    public UnityEvent Event;

    public InputField InputField;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if(InputField.text.Length>0)
                Event?.Invoke();
            else
                ErrorView.SetActive(true);
        });
    }

  
}
